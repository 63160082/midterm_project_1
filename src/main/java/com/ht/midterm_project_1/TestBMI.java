/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.midterm_project_1;

/**
 *
 * @author ACER
 */
public class TestBMI {
    public static void main(String[] args) {
        BMI bmi1 = new BMI(26 , 1.23);      // สร้าง object และกำหนดค่าเข้าไป
        bmi1.print();       //เรียก method print และแสดงค่า
        System.out.println("--------------------------------------------------------");
        bmi1.setBMI(32, 1.31);  // เรียกใช้ setter
        bmi1.print();
        System.out.println("--------------------------------------------------------");
        bmi1.setBMI(0, 1.50);
        bmi1.print();
    }
}
