/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.midterm_project_1;

/**
 *
 * @author ACER
 */
public class BMI {
    private double weight;      // หน่วยเป็น kg
    private double height;      // หน่วยเป็น m

    public BMI(double weight, double height) {      // Constructor รับค่า parameter
        this.weight = weight;
        this.height = height;
    }
    
    public double calBMI() {        // คำนวณค่าดัชนีมวลกาย
        return weight / (height * height);
    }

    public double getWeight() {     
        return this.weight;
    }

    public double getHeight() {
        return this.height;
    }

    public void setBMI(double weight , double height) {     // check ค่า weight และ height     
        if(weight <= 0 || height <= 0) {        //ค่า weight หรือ height เท่ากับ 0 หรือน้อยกว่า 0
            System.out.println("WEIGHT OR HEIGHT Must More Than 0 !!!");
            return;
        }       // ค่า weight , height มากกว่า 0
        this.weight = weight;
        this.height = height;       
    }
    
    public void print() {       //method แสดงผลลัพธ์
        System.out.println("Weight : " + this.getWeight() +  " kg. " + "Height : " + this.getHeight() + " m. " + "BMI is " + this.calBMI() );
    }
}
    
    
    
    
 
